#ifndef CIRCUNFERENCIA_H
#define CIRCUNFERENCIA_H
#include "formageometrica.hpp"

using namespace std;

class Circunferencia : public FormaGeometrica {

public:
  Circunferencia();
  Circunferencia(float largura);


  float calculaArea();
  float calculaPerimetro();
};
#endif
