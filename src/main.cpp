#include <iostream>
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circunferencia.hpp"

using namespace std;

int main() {

	Quadrado * forma1 = new Quadrado();

	float largura;

	cout << "Lado do Quadrado: " << endl;
	cin >> largura;

	forma1->setLargura(largura);
	forma1->calculaArea();
	forma1->calculaPerimetro();

	Triangulo * forma2 = new Triangulo();

	float altura;

	cout << "Altura do Triangulo: " << endl;
	cin >> altura;
	cout << "Base do Triangulo: " << endl;
	cin >> largura;

	forma2->setLargura(largura);
	forma2->setAltura(altura);
	forma2->calculaArea();
	forma2->calculaPerimetro();

	Circunferencia * forma3= new Circunferencia();

	cout << "Raio da Circunferencia: " << endl;
	cin >> largura;

	forma3->setLargura(largura);
	forma3->calculaArea();
	forma3->calculaPerimetro();

	cout << "\nÁrea do Quadrado: " << forma1->getArea() << endl;
	cout << "Perímetro do Quadrado: " << forma1->getPerimetro() << endl;
	cout << "----------------------------" << endl;
	cout << "Área do Triangulo: " << forma2->getArea() << endl;
	cout << "Perímetro do Triangulo: " << forma2->getPerimetro() << endl;
	cout << "----------------------------" << endl;
	cout << "Área da Circunferencia: " << forma3->getArea() << endl;
	cout << "Perímetro da Circunferencia: " << forma3->getPerimetro() << endl;

	delete(forma1);
	delete(forma2);
	delete(forma3);
}
