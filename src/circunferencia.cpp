#include <iostream>
#include "circunferencia.hpp"
#define PI 3.1415926536f

Circunferencia::Circunferencia() {
	setLargura(0);
}
Circunferencia::Circunferencia(float largura) {

	setLargura(largura);

}

float Circunferencia::calculaArea() {

	setArea(getLargura() * getLargura() * (PI));
	return getArea();
}

float Circunferencia::calculaPerimetro() {
	setPerimetro(getLargura() * (PI) * 2.0f);
	return getPerimetro();
}
